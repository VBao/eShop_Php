<?php

namespace App\Models\Province;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Streets extends Model
{
    use HasFactory;

    protected $table = "street";
}
