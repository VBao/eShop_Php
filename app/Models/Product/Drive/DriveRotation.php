<?php

namespace App\Models\Product\Drive;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DriveRotation extends Model
{
    use HasFactory;
}
