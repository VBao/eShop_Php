<?php

namespace App\Models\Product\Drive;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DriveWrite extends Model
{
    use HasFactory;
}
